﻿using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

/*
	Silent tools!
	Current tools:

	- Reorder humanoid bones for VRchat
	Iterates over humanoid bones and sets their sibling index to 0. 
	For some funny reason, this does not break prefabs.

	ref: 
	https://docs.unity3d.com/ScriptReference/HumanBodyBones.html
	https://docs.unity3d.com/ScriptReference/Animator.GetBoneTransform.html

	- Automatically set up LODGroups
	Sets up an LODGroup where the object is culled at 1%, 3%, or 5% visibility.
	Added as a context menu item to the LODGroups component, which is easier to work with.

    - Convert to Mesh Renderer and Filter
    This is handy for when you have set up a scene with a bunch of skinned mesh renderers
    that could be mesh renderers, but have unpacked the prefabs. This is only a conversion,
    so skinning will no longer be applied.
    
    - Search and Remap All Materials
    This is handy for when you have several FBX prefabs that can automatically assign
    their own materials, but would normally need it to be done individually.
    
    - Revert Transform to Prefab Default
    When importing FBX prefabs, they can come with transforms applied to the root object.
    However, Unity considers these "default overrides" which can't be reverted in mass.

	Special thanks to:
	Lyuma

*/
	
namespace SilentTools
{
#if UNITY_EDITOR
public class SilentTools : EditorWindow 
{

    [MenuItem ("CONTEXT/Animator/Reorder bones")]
    public static void ReorderHumanoidBones (MenuCommand command) {
    	Animator anim = (Animator)command.context;
    	HumanBodyBones[] humanBones = (HumanBodyBones[]) Enum.GetValues(typeof(HumanBodyBones));

    	foreach (HumanBodyBones hBone in humanBones)
    	{
    		if (hBone == HumanBodyBones.LastBone) continue;

			Transform boneT = anim.GetBoneTransform(hBone);
			if (boneT != null) boneT.SetSiblingIndex(0);
    	}
    }

    public static void CreateSimpleCullingGroup (MenuCommand command, float range) {
    	LODGroup group = (LODGroup)command.context;
    	
    	Renderer[] targets = (Renderer[])group.GetComponentsInChildren<Renderer>();

        LOD[] lods = new LOD[2];
        lods[0] = new LOD(1.0F, targets);
        lods[1] = new LOD(range, targets);

		group.SetLODs(lods);
		group.RecalculateBounds();
		
		// Set the fading params here. Using None is the cheapest. 
		// Modify this to suit your needs. 
		group.fadeMode = LODFadeMode.None;
		group.animateCrossFading = false;
		
    }

    [MenuItem ("CONTEXT/LODGroup/Create simple culling group (1%)")]
    public static void CreateSimpleCullingGroup1 (MenuCommand command) {
    	CreateSimpleCullingGroup(command, 0.01F);
    }
    [MenuItem ("CONTEXT/LODGroup/Create simple culling group (3%)")]
    public static void CreateSimpleCullingGroup3 (MenuCommand command) {
    	CreateSimpleCullingGroup(command, 0.03F);
    }
    [MenuItem ("CONTEXT/LODGroup/Create simple culling group (5%)")]
    public static void CreateSimpleCullingGroup5 (MenuCommand command) {
    	CreateSimpleCullingGroup(command, 0.05F);
    }

    [MenuItem ("CONTEXT/SkinnedMeshRenderer/Convert to Mesh Renderer and Filter", false, 141)]
    public static void ConvertSkinnedToNormal (MenuCommand command)
    {
        Mesh sourceMesh;
        MeshRenderer mr = null;
        MeshFilter mf = null;
        SkinnedMeshRenderer smr = null;
        GameObject go = null;
        Material[] smat = null;

        if (command.context is SkinnedMeshRenderer) {
            smr = command.context as SkinnedMeshRenderer;
            sourceMesh = smr.sharedMesh;
            go = smr.gameObject;
            smat = smr.sharedMaterials;
        } else {
            EditorUtility.DisplayDialog ("Convert to Mesh Renderer and Filter", "Unknkown context type " + command.context.GetType ().FullName, "OK", "");
            throw new NotSupportedException ("Unknkown context type " + command.context.GetType ().FullName);
        }
 
        if (smr != null) {
            Undo.DestroyObjectImmediate (smr);
            mf = Undo.AddComponent<MeshFilter>(go);
            mf.sharedMesh = sourceMesh;
            mr = Undo.AddComponent<MeshRenderer>(go);
            mr.sharedMaterials = smat;
        }
    }

    [MenuItem ("CONTEXT/FBXImporter/Search and Remap All Materials")]
    public static void SearchAndRemapAllFBXMaterials (MenuCommand command) {
        ModelImporter importer = (ModelImporter)command.context;
        
        importer.SearchAndRemapMaterials(
            (ModelImporterMaterialName)0, 
            (ModelImporterMaterialSearch)1
        );

        importer.SearchAndRemapMaterials(
            (ModelImporterMaterialName)1, 
            (ModelImporterMaterialSearch)1
        );
        importer.SearchAndRemapMaterials(
            (ModelImporterMaterialName)2, 
            (ModelImporterMaterialSearch)1
        );
        
    }

    [MenuItem ("CONTEXT/Transform/Revert Transform to Prefab Default")]
    public static void RevertTransformDefault (MenuCommand command) {
        Transform tran = (Transform)command.context;

        SerializedObject soTran = new UnityEditor.SerializedObject(tran);

        PrefabUtility.RevertPropertyOverride(soTran.FindProperty("m_LocalPosition"), InteractionMode.AutomatedAction);
        PrefabUtility.RevertPropertyOverride(soTran.FindProperty("m_LocalRotation"), InteractionMode.AutomatedAction);
        PrefabUtility.RevertPropertyOverride(soTran.FindProperty("m_LocalScale"), InteractionMode.AutomatedAction);
    }
}
#endif
    
}