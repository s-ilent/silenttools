# Silent Tools!

A script with handy functions for VRchat.
# [Can't find the Download link? Click here!](https://gitlab.com/s-ilent/silenttools/-/archive/master/silenttools-master.zip)

### Current Tools

* **Reorder humanoid bones for VRchat**

	Iterates over humanoid bones and sets their sibling index to 0. 
	For some funny reason, this does not break prefabs.

	To use it, right-click an avatar's animator to reveal the Reorder bones option.

	![](https://cdn.discordapp.com/attachments/461086854049955840/744159895833804920/2020-08-15_21-07-05.webm)

* **Automatically set up LODGroups**

	Sets up an LODGroup where the object is culled at 1%, 3%, or 5% visibility.
	Added as a context menu item to the LODGroups component, which is easier to work with.

* **Convert to Mesh Renderer and Filter**

    This is handy for when you have set up a scene with a bunch of skinned mesh renderers
    that could be mesh renderers, but have unpacked the prefabs. This is only a conversion,
    so skinning will no longer be applied.

* **Search and Remap All Materials**

    This is handy for when you have several FBX prefabs that can automatically assign
    their own materials, but would normally need it to be done individually.
    
* **Revert Transform to Prefab Default**

    When importing FBX prefabs, they can come with transforms applied to the root object.
    However, Unity considers these "default overrides" which can't be reverted in mass.

### Installation Instructions
Copy the contents of the Assets folder to your project's Assets folder. 

### Special Thanks
* Lyuma